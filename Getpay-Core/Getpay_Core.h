//
//  Getpay_Core.h
//  Getpay-Core
//
//  Created by Leandro Lopes on 11/02/20.
//  Copyright © 2020 Getnet. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Getpay_Core.
FOUNDATION_EXPORT double Getpay_CoreVersionNumber;

//! Project version string for Getpay_Core.
FOUNDATION_EXPORT const unsigned char Getpay_CoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Getpay_Core/PublicHeader.h>


